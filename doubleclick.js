import './style.css';
import {Map, View} from 'ol';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';
import {
  DblClickDragZoom,
  defaults as defaultInteractions,
} from 'ol/interaction.js';

const map = new Map({
  interactions: defaultInteractions().extend([new DblClickDragZoom()]),
  layers: [
    new TileLayer({
        source: new OSM(),
    }),
  ],
  target: 'map',
  view: new View({
    center: [-99.12766, 19.42847],
    projection: "EPSG:4326",
    zoom: 13.5
  })
});

